﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NetCoreWebApp.Models
{
    public class Employee
    {
        [DisplayName("ID")]
        public string Id { get; set; }

        [DisplayName("名前")]
        public string Name { get; set; }

        [DisplayName("表示名")]
        public string DisplayName { get; set; }

        [DisplayName("退職済")]
        public bool Retired { get; set; }

        [DisplayName("最終更新日")]
        public DateTime LastUpdate { get; set; }

    }
}
