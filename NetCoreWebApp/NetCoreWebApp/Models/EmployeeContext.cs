﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace NetCoreWebApp.Models
{
    public class EmployeeContext : DbContext
    {
        public DbSet<Employee> Emploees { get; set; }



        public EmployeeContext()
        { }

        public EmployeeContext(DbContextOptions<EmployeeContext> options) : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("DataSource=app.db");

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = "";
            services.AddDbContext<EmployeeContext>(options => options.UseSqlite(connectionString));
            services.AddControllers();
        }
    }
}
